﻿using System;
using System.Collections.Generic;
using System.Linq;
using CMS.Domain.Entities;
using CMS.Domain.Abstract;

namespace CMS.Domain.Concrete
{
    public class EFPageRepository:IPageRepository
    {
        private EFDbContext db = new EFDbContext();

        public IQueryable<Page> Pages { get { return db.Pages; } }

        public Page GetbyID(int pageID)
        {
            return db.Pages.Find(pageID);
        }

        public Page GetbyAlias(string alias)
        {
            return db.Pages.FirstOrDefault(x => x.Alias == alias);
        }

        public bool Add(Page page)
        {
            var entry = db.Pages.FirstOrDefault(x => x.Alias == page.Alias);
            if (entry != null)
                return false;

            page.CreationDate = DateTime.Now;
            page.LastChangeDate = page.CreationDate;
            db.Pages.Add(page);
            return true;
        }

        public bool Update(Page page)
        {
            var entry = db.Pages.Find(page.PageID);
            if (entry == null)
                return false;
            
            if (entry.Alias!=page.Alias)
            {
                var entry2 = db.Pages.FirstOrDefault(x => x.Alias == page.Alias);
                if (entry2 != null)
                    return false;
                entry.Alias = page.Alias;
            }
            entry.Description = page.Description;
            entry.Keywords = page.Keywords;
            entry.Title = page.Title;
            entry.LastChangeDate = DateTime.Now;
            entry.Author = page.Author;
            return true;
        }

        public bool Delete(int pageID)
        {
            var entry = db.Pages.Find(pageID);
            if (entry != null)
            {
                db.Pages.Remove(entry);
                return true;
            }
            return false;
        }

        public bool Copy(int pageID)
        {
            var entry = db.Pages.Find(pageID);
            if (entry!=null)
            {
                return Add(new Page { Alias = entry.Alias + "2", Author = entry.Author, CreationDate = DateTime.Now, LastChangeDate = DateTime.Now, Description = entry.Description, Keywords = entry.Keywords, Title = entry.Title, Status = entry.Status });
            }
            return false;
        }

        public int Commit()
        {
            return db.SaveChanges();
        }

        public bool ChangeStatus(int pageID, bool status)
        {
            var entry = db.Pages.Find(pageID);
            if (entry!=null)
            {
                entry.Status = status;
                return true;
            }
            return false;
        }

        public bool ChangeStatus(int pageID)
        {
            var entry = db.Pages.Find(pageID);
            if (entry != null)
            {
                entry.Status = !entry.Status;
                return true;
            }
            return false;
        }
    }
}
