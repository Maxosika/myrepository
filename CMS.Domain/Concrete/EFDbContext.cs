﻿using System;
using System.Collections.Generic;
using System.Linq;
using CMS.Domain.Entities;
using System.Data.Entity;

namespace CMS.Domain.Concrete
{
    public class EFDbContext:DbContext
    {
        public EFDbContext()
        {

        }

        public DbSet<Page> Pages { get; set; }
        public DbSet<Service> Services { get; set; }
    }
}
