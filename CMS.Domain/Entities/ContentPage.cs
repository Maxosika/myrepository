﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Domain.Entities
{
    public class ContentPage
    {
        public int ContentPageID { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public virtual ICollection<ContentBlock> Blocks { get; set; }
        public virtual Page Page { get; set; }
    }
}
