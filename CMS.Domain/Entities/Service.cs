﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CMS.Domain.Entities
{
    public class Service
    {
        public int ServiceID { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public string ShortText { get; set; }

        public virtual Image ImageInner { get; set; }

        public virtual Image ImageMain { get; set; }
        [Required]
        public virtual Page Page { get; set; }

        //public ICollection<string> Tags { get; set; }
        //public ICollection<string> Projects { get; set; }
    }
}
