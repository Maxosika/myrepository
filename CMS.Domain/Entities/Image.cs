﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CMS.Domain.Entities
{
    public class Image
    {
        public int ImageID { get; set; }
        public string Title { get; set; }
        public string Alt { get; set; }

        [Required]
        public string Url { get; set; }
    }
}
