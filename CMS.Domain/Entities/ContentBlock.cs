﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Domain.Entities
{
    public class ContentBlock
    {
        public int ContentBlockID { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public virtual Image Image { get; set; }
    }
}
