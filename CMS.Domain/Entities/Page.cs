﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;

namespace CMS.Domain.Entities
{
    public class Page
    {
        [HiddenInput(DisplayValue=false)]
        public int PageID { get; set; }

        [Required(ErrorMessage="Необходимо заполнить поле \"Ссылка\".")]
        [RegularExpression("[A-Za-z0-9-]+", ErrorMessage="В ссылке допускаются только английские буквы, цифры и знак тире.")]
        [MinLength(4, ErrorMessage="Длина ссылки не может быть меньше 4 символов.")]
        [Display(Name="Ссылка")]
        public string Alias { get; set; }

        [Display(Name = "Название")]
        public string Title { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Ключевые слова")]
        public string Keywords { get; set; }

        [Display(Name = "Дата создания")]
        public DateTime CreationDate { get; set; }

        [Display(Name = "Дата изменения")]
        public DateTime LastChangeDate { get; set; }

        [Display(Name = "Автор")]
        public string Author { get; set; }
        public bool Status { get; set; }
    }
}
