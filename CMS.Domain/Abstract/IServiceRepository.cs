﻿using System;
using System.Linq;
using CMS.Domain.Entities;

namespace CMS.Domain.Abstract
{
    public interface IServiceRepository
    {
        IQueryable<Service> Services { get; }

        Service GetbyID(int pageID);
        Service GetbyAlias(string alias);
        bool Add(Page page);
        bool Update(Page page);
        bool Delete(int pageID);
        bool Copy(int pageID);
        bool ChangeStatus(int pageID);
        bool ChangeStatus(int pageID, bool status);
        int Commit();
    }
}
