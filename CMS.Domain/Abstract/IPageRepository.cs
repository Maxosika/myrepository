﻿using System.Linq;
using CMS.Domain.Entities;

namespace CMS.Domain.Abstract
{
    public interface IPageRepository
    {
        IQueryable<Page> Pages { get; }
        Page GetbyID(int pageID);
        Page GetbyAlias(string alias);
        bool Add(Page page);
        bool Update(Page page);
        bool Delete(int pageID);
        bool Copy(int pageID);
        bool ChangeStatus(int pageID);
        bool ChangeStatus(int pageID, bool status);
        int Commit();

    }
}
