﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CMS.WebUI.HtmlHelpers
{
    public class BreadCrumb
    {
        public string Url { get; set; }
        public string Title { get; set; }
    }

    public static class Breadcrumbs
    {
        //public string homeicon="<i class=\"fa fa-home\"></i>";

        public static IHtmlString AdminCrumb(this HtmlHelper html, string url)
        {
            string[] segments = url.Split('/');
            string Html = "<ul class=\"breadcrumb no-border no-radius b-b b-light pull-in\">";
            if (segments.Count()==2 && segments[1].ToLower()=="admin")
            {
                Html += "<li class=\"active\"><a href=\"\admin\"><i class=\"fa fa-home\"></i> Главная</a></li>";
            }
            else
            {
                Html += "<li><a href=\"index.html\"><i class=\"fa fa-home\"></i> Главная</a></li>";
                    Html += "<li><a href=\"#\">" + segments[2] + "</a></li>";
                Html += "<li class=\"active\"><a href=\"#\">" + segments.Last() + "</a></li>";
            }
            
            Html += "</ul>";
            return MvcHtmlString.Create(Html);
        }  
              
    }
}