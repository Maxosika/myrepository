﻿using System;
using Ninject;
using CMS.Domain.Abstract;
using CMS.Domain.Concrete;
using System.Web.Routing;
using System.Web.Mvc;

namespace CMS.WebUI.Infrastructure
{
    public class NinjectControllerFactory:DefaultControllerFactory
    {
        private IKernel NinjectKernel;

        public NinjectControllerFactory()
        {
            NinjectKernel = new StandardKernel();
            AddBindings();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)NinjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            NinjectKernel.Bind<IPageRepository>().To<EFPageRepository>();
            NinjectKernel.Bind<IServiceRepository>().To<EFServiceRepository>();
        }
    }
}