﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Domain.Entities;
using CMS.Domain.Abstract;

namespace CMS.WebUI.Controllers
{
    public class PageController : Controller
    {
        private IPageRepository repository;

        public PageController(IPageRepository repo)
        {
            repository = repo;
        }

        public ActionResult Index()
        {
            var result = repository.Pages.ToList();
            if (result.Count() == 0)
                result = new List<Page>();
            return View(result);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Page page, string action)
        {
            if (ModelState.IsValid)
            {
                if (!repository.Add(page))
                {
                    TempData["message"] = "Ссылка не уникальна. Введите другую ссылку.";
                    return View(page);
                }
                else
                    repository.Commit();

                if (action == "save")
                    return View ("Edit", page);

                return RedirectToAction("Index");

            }
            else
                return View(page);
        }

        public ActionResult Edit(int pageID)
        {
            return View(repository.GetbyID(pageID));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Page page, string action)
        {
            if (ModelState.IsValid)
            {
                repository.Update(page);
                repository.Commit();

                if (action == "return")
                    return RedirectToAction("Index");
            }
            return View(page);
        }

        public ActionResult Delete(int pageID)
        {
            repository.Delete(pageID);
            repository.Commit();
            return RedirectToAction("Index");
        }

        public ActionResult Copy(int pageID)
        {
            repository.Copy(pageID);
            repository.Commit();
            return RedirectToAction("Index");
        }

        public ActionResult Publish(int pageID)
        {
            repository.ChangeStatus(pageID);
            repository.Commit();
            return RedirectToAction("Index");
        }

        public ActionResult Save()
        {
            return RedirectToAction("Index");
        }

    }
}