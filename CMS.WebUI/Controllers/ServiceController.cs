﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CMS.Domain.Abstract;
using CMS.Domain.Entities;

namespace CMS.WebUI.Controllers
{
    public class ServiceController : Controller
    {
        private IServiceRepository repository;

        public ServiceController(IServiceRepository repo)
        {
            repository = repo;
        }
        
        public ActionResult Index()
        {
            var result = repository.Services.ToList();
            if (result.Count() == 0)
                result = new List<Service>();
            return View(result);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Service service, string action)
        {
            return View();
        }

        public ActionResult Edit()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Edit(Service serivce, string action)
        {
            return View();
        }

        public ActionResult Copy()
        {
            return RedirectToAction("Index");
        }

        public ActionResult Delete()
        {
            return RedirectToAction("Index");
        }

        public ActionResult ChangeStatus()
        {
            return RedirectToAction("Index");
        }

	}
}